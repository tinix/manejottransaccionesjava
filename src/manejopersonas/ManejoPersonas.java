package manejopersonas;

import datos.PersonasJDBC;
import java.sql.*;
import datos.Conexion;

public class ManejoPersonas {
	
	public static void main(String[] args) {
		PersonasJDBC personasJDBC = new PersonasJDBC();
		
		Connection conn = null;
		try {
			conn = Conexion.getConnection();
			if (conn.getAutoCommit()) {
				conn.setAutoCommit(false);
			}
			PersonasJDBC personas = new PersonasJDBC(conn);
			personas.update(2, "Regreso2", "Regreso");
			personas.insert("Miguel12", "Ayala12345678901234567890123456789012345678901234567890");
			conn.commit();
		} catch (SQLException e) {
			try {
				System.out.println("Entramos al rollback");
				e.printStackTrace(System.out);
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace(System.out);
			}
		}
	}

}
